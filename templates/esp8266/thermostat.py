def thermostat(self, function=''):

    if function == 'run':

        from machine import unique_id
        from ubinascii import hexlify
        self.status['id'] = hexlify(unique_id()).decode()

        import ds18x20
        from machine import Pin
        from onewire import OneWire
        from time import sleep_ms
        PIN_SENSOR = 4
        pin_onewire = OneWire(Pin(PIN_SENSOR))
        sensor = ds18x20.DS18X20(pin_onewire)
        sensors = sensor.scan()

        ALPHA = 0.125
        PIN_RELAY = 5
        SPAN = 1.618
        self.status['threshold'] = 16.382
        relay = Pin(PIN_RELAY, Pin.OUT)

        import network
        SSID = 'CableComm-61624c'
        PASSPHRASE = '225522662'
        ap = network.WLAN(network.AP_IF)
        ap.active(False)
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        if not wlan.isconnected():
            wlan.connect(SSID, PASSPHRASE)
            while not wlan.isconnected():
                pass

        import socket
        BUFFER_SIZE = 4
        PIN_LED = 2
        PORT_NO = 3181
        led = Pin(PIN_LED, Pin.OUT)
        led.value(1)
        sock_inc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_inc.bind(('', PORT_NO))
        SERVER_ADDR = '192.168.1.99'

        from machine import Timer
        vt_state_adjust = Timer(-1)
        ADJUST_PERIOD = 8000
        vt_state_adjust.init(period=ADJUST_PERIOD, mode=Timer.PERIODIC,
                             callback=lambda t: state_adjust())

        def state_adjust():

            try:

                sock_out = socket.socket(socket.AF_INET,
                                         socket.SOCK_STREAM)
                sock_out.connect((SERVER_ADDR, PORT_NO))
                sock_out.send(self.status['id'])
                self.status['mode'], self.status['threshold'] = sock_out.recv(BUFFER_SIZE).split()
                print(self.status)
                sock_out.close()

            except OSError:

                pass

            while self.status['commands']:

                print(self.status['commands'])

                try:

                    command = self.status['commands'].pop(0).decode()
                    self.status['threshold'] = float(command)

                except ValueError:

                    if command == 'T':

                        self.status['mode'] = 1

                    elif command == 'F':

                        relay.value(0)
                        self.status['mode'] = 0
                        del self.status['reading']

                except TypeError:

                    print('TypeError', command)

                except IndexError:

                    pass

            if self.status['mode'] == 1:

                measure_temp()

                if self.status['reading'] <= self.status['threshold']:

                    relay.value(1)
                    self.status['state'] = 1

                elif self.status[
                        'reading'] > self.status['threshold']+SPAN:

                    relay.value(0)
                    self.status['state'] = 0

        def measure_temp():

            sensor.convert_temp()
            sleep_ms(750)
            try:

                self.status['reading'] = (
                        (1-ALPHA)*self.status['reading'])+(
                                ALPHA*sensor.read_temp(sensors[0]))
            except KeyError:

                self.status['reading'] = sensor.read_temp(sensors[0])

        while True:

            sock_inc_ = sock_inc.recvfrom(BUFFER_SIZE)
            self.status['commands'].append(sock_inc_[0])
