def generic(self, function=''):

    if function == 'run':

        def command_processing():

            while True:

                command = self.status['command_queue'].get()
                for word in command.split():
                    print(word)

        def input_loop():

            from time import sleep

            while True:

                self.status['command_queue'].put(input('?_'))
                while not self.status['command_queue'].empty():
                    pass
                sleep(0.125)

        def main():

            pass  # take action depending on 'state'

        def socket_listen(buffersize=1, port=3181):

            from select import select
            import socket

            #try:

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.setblocking(0)
            #self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            self.socket.bind(('', port))

            while True:

                incoming_on_socket = select([self.socket], [], [], 12)
                if incoming_on_socket[0]:
                    #incoming_data = self.socket.recvfrom(self.buffersize)
                    self.status['command_queue'].put(
                        self.socket.recvfrom(buffersize))

            #finally:
                #self.socket.close()

        from threading import Thread
        from queue import Queue
        self.status['command_queue'] = Queue()

        thread_command_processing = Thread(name='command_processing',
                                           target=command_processing)
        thread_command_processing.start()

        thread_input_loop = Thread(name='input_loop', target=input_loop)
        thread_input_loop.start()

        thread_main = Thread(name='main', target=main)
        thread_main.start()

        thread_socket_listen = Thread(name='socket_listen',
                                      target=socket_listen)
        thread_socket_listen.start()
