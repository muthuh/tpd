def generic_peer(self, function=''):

    if function == 'run':

        def command_processing():

            while True:

                command = self.status['commands'].get()
                for word in command.split():
                    pass

        def input_loop():

            while True:

                self.status['commands'].put(input('?_'))
                
        def main():

            pass
        
        def socket_listen(buffersize=1, port=3181):

            import socket

            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setblocking(0)
            #self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.bind(('', port))

            while True:

                sock_inc_ = sock.recvfrom(BUFFER_SIZE)
                self.status['commands'].append(sock_inc_[0])

        from threading import Thread

        thread_command_processing = Thread(name='command_processing',
                                           target=command_processing)
        thread_command_processing.start()

        thread_input_loop = Thread(name='input_loop', target=input_loop)
        thread_input_loop.start()

        #thread_main = Thread(name='main', target=main)
        #thread_main.start()

        thread_socket_listen = Thread(name='socket_listen',
                                      target=socket_listen)
        thread_socket_listen.start()
