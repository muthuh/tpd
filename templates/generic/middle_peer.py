def middle_peer(self, function=''):

    if function == 'run':

        def command_processing():

            while True:

                command = self.status['commands'].get()
                print(command)

        def input_loop():

            while True:

                self.status['commands'].put(input('?_'))

        def main():

            pass

        def socket_listen(buffersize=8, port=3181):

            import socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(('', port))
            sock.listen()

            import sqlite3
            db_connection = sqlite3.connect('data/tpd.db')
            db_cursor = db_connection.cursor()
            db_cursor.execute('CREATE TABLE IF NOT EXISTS peers(id TEXT NOT NULL PRIMARY KEY, mode INT, threshold FLOAT)')
            db_connection.commit()
            #db_connection.close()

            while True:

                conn, addr = sock.accept()
                print('{}'.format(addr))
                incoming = conn.recv(buffersize).decode()
                db_cursor.execute("SELECT mode, threshold FROM peers WHERE id = ?", (incoming,))
                query_result = db_cursor.fetchone()
                if query_result is not None:
                    conn.send('{} {}'.format(query_result[0],
                              query_result[1]).encode())

        from threading import Thread

        thread_command_processing = Thread(name='command_processing',
                                           target=command_processing)
        thread_command_processing.start()

        thread_input_loop = Thread(name='input_loop', target=input_loop)
        thread_input_loop.start()

        thread_main = Thread(name='main', target=main)
        thread_main.start()

        thread_socket_listen = Thread(name='socket_listen',
                                      target=socket_listen)
        thread_socket_listen.start()
