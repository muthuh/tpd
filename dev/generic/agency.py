def agency(self, function=''):

    if function == 'run':

        def command_processing():

            while True:
                self.status['commands'].get()

        def input_loop():

            while True:
                self.status['commands'].put(input('?_'))

        def main():

            import sqlite3
            db_connection = sqlite3.connect('/tmp/tpd.db')
            db_cursor = db_connection.cursor()
            db_cursor.execute("""CREATE TABLE IF NOT EXISTS households (
                              address TEXT PRIMARY KEY,
                              lock BLOB)""")
            db_cursor.execute("""CREATE TABLE IF NOT EXISTS tenants (
                              name TEXT PRIMARY KEY,
                              household_id TEXT,
                              key BLOB,
                              FOREIGN KEY (household_id)
                              REFERENCES households(id))""")
            db_cursor.execute("""CREATE TABLE IF NOT EXISTS appliances (
                              id TEXT PRIMARY KEY,
                              household_id TEXT,
                              mode INT, role TEXT,
                              FOREIGN KEY (household_id)
                              REFERENCES households(id))""")
            db_connection.commit()
            #db_connection.close()

            import json
            import re
            #try:
            #    import bottle
            #except ImportError:
            from lib import bottle

            @bottle.route('/household', method=['POST'])
            def household_handler():

                try:

                    if bottle.request is None:
                        raise ValueError

                    if bottle.request.method == 'POST':

                        db_cursor.execute("""SELECT address
                                          FROM households
                                          WHERE address=?""",
                                          (bottle.request.query.address,))

                        if db_cursor.fetchone() is None:

                            from os import urandom
                            db_cursor.execute("""INSERT INTO households
                                              VALUES (?, ?)""",
                                              (bottle.request.query.address,
                                               urandom(16)))
                            db_connection.commit()
                            print(bottle.request.query.address)

                        else:
                            raise KeyError

                except ValueError:
                    bottle.response.status = 400
                    return
                except KeyError:
                    bottle.response.status = 405
                    return

                #bottle.response.headers['Content-Type'] = 'application/json'
                #return json.dumps({'some': 'some'})

            #app = application = bottle.default_app()
            bottle.run(host='127.0.0.1')

        def socket_listen(buffersize=8, port=3181):

            import socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(('', port))
            sock.listen()

            while True:

                conn, addr = sock.accept()
                print('{}'.format(addr))
                incoming = conn.recv(buffersize).decode()
                db_cursor.execute("SELECT status, threshold FROM households WHERE id = ?", (incoming,))
                query_result = db_cursor.fetchone()
                if query_result is not None:
                    conn.send('{} {}'.format(query_result[0],
                              query_result[1]).encode())

        from threading import Thread

        thread_command_processing = Thread(name='command_processing',
                                           target=command_processing)
        thread_command_processing.start()

        thread_input_loop = Thread(name='input_loop', target=input_loop)
        thread_input_loop.start()

        thread_main = Thread(name='main', target=main)
        thread_main.start()

        thread_socket_listen = Thread(name='socket_listen',
                                      target=socket_listen)
        thread_socket_listen.start()
