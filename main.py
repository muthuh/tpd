#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Actor():

    def __init__(self, role='appliance', device='generic'):

        self.status = {'role': role, 'mode': 0}
        try:
            from queue import Queue
            self.status['commands'] = Queue()
        except ImportError:
            self.status['commands'] = []

        exec('from dev{}.{} import {}'.format
             ('.{}'.format(device), role, role), globals())

        exec('this_module = {}'.format(role), globals())
        setattr(self, role, this_module)

        exec('self.{}(self, {})'.format
             (role, '\'{}\''.format(role)), {'self': self})
        exec('self.{}(self, {})'.format(role, '\'run\''), {'self': self})


if __name__ == "__main__":

    actor = Actor('agency')
