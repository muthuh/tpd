#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Peer():

    def __init__(self, template='peer', device='generic'):

        self.status = {'functionality': template, 'mode': 0}
        try:
            from queue import Queue
            self.status['commands'] = Queue()
        except ImportError:
            self.status['commands'] = []

        exec('from templates{}.{} import {}'.format
             ('.{}'.format(device), template, template), globals())

        exec('this_module = {}'.format(template), globals())
        setattr(self, template, this_module)

        exec('self.{}(self, {})'.format
             (template, '\'{}\''.format(template)), {'self': self})
        exec('self.{}(self, {})'.format(template, '\'run\''), {'self': self})


if __name__ == "__main__":

    peer = Peer()
